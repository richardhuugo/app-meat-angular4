import {Restaurant} from './restaurant/restaurant.model'
import {API_MEAT} from '../../../app.api'
import { Injectable} from '@angular/core'
import { Observable} from 'rxjs/Observable'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/catch'
import { Http} from '@angular/http'
import {Error} from '../app.error'
@Injectable()
export class RestaurantsService  {
   
    constructor(private http: Http){}

    restaurants(): Observable<Restaurant[]>  {
        return this.http.get(`${API_MEAT}/restaurants`)
        .map(response => response.json())
        .catch(Error.Error)
    }

    restaurantById(id: string): Observable<Restaurant>{
        return this.http.get(`${API_MEAT}/restaurants/${id}`)
        .map(response => response.json())
        .catch(Error.Error)
    }
}