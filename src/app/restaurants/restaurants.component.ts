import { Component, OnInit, Injectable } from '@angular/core';
import {Restaurant} from './restaurant/restaurant.model'
import {RestaurantsService} from './restaurant.service'
@Component({
  selector: 'mt-restaurants',
  templateUrl: './restaurants.component.html'
})


export class RestaurantsComponent implements OnInit {

  restaurant: Restaurant[] 
  constructor(private restaurantsService: RestaurantsService ) { 

  }

  ngOnInit() {
    this.restaurantsService.restaurants().subscribe( restaurant => this.restaurant = restaurant)
  }

}
