import {Response} from '@angular/http'
import { Observable } from 'rxjs/Observable';

export class Error {
    static Error (error: Response | any) {
        let errorMensagem: string
        if(error instanceof Response){
            errorMensagem = `Erro ${error.status} ao acessar url ${error.url} ${error.statusText}`
        }else {
            errorMensagem = error.toString()
        }
        console.log(errorMensagem)
        return Observable.throw(errorMensagem)
    }
}