# Meat app start

## 1. Passos para começar

### Clonando o Repositório

`git clone https://richardhuugo@bitbucket.org/richardhuugo/app-meat-angular4.git`

### Instalando as Dependências

`npm install`

### Inicializando o Servidor

`ng serve` ou `npm start`

